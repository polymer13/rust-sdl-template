extern crate sdl2;

use std::time::Duration;
use std::thread::sleep;
use std::rc::Rc;

#[allow(dead_code)]
struct AppCore {
    sdl_ctx: sdl2::Sdl,
    sdl_video: sdl2::VideoSubsystem,
    window_ctx: Rc<sdl2::video::WindowContext>,
    canvas: sdl2::render::Canvas<sdl2::video::Window>,
}

struct AppConf {
    window_title: String,
    window_resolution: (u32, u32),
}

impl AppCore {

    pub fn init(conf: AppConf) -> Result<Self, String> {

        use sdl2::video::WindowBuildError as Wbe;
        use sdl2::IntegerOrSdlError as Iose;

        let sdl = sdl2::init()?;
        let video = sdl.video()?;
        let win = match video.window(
            conf.window_title.as_str(),
            conf.window_resolution.0,
            conf.window_resolution.1,
        ).position_centered()
         .build() {

             Ok(win) => win,
             Err(err) => match err {
                 Wbe::HeightOverflows(i) =>
                     return Err(format!( "Window height is too large: {}", i) ),
                 Wbe::WidthOverflows(i) =>
                     return Err(format!( "Window width is too large: {}", i) ),
                 Wbe::InvalidTitle(_) =>
                     return Err("Window title is invalid!".to_owned()),
                 Wbe::SdlError(err) =>
                     return Err(format!("SDL internal error: {}", err)),
             }
         };

        let win_ctx = win.context();

        let c = match win.into_canvas().build() {
            Ok(c) => c,
            Err(err) => match err {
                Iose::SdlError(err) =>
                    return Err(format!("SDL internal error during canvas creation: {}", err)),
                Iose::IntegerOverflows(e, n) =>
                    return Err(format!("SDL encountered integer overflow during canvas creation: {} VALUE({})", e, n)),
            }
        };

        Ok( Self {
            sdl_ctx: sdl,
            sdl_video: video,
            window_ctx: win_ctx,
            canvas: c,
        })
    }

}


fn main() {

    use sdl2::pixels::Color;

    let conf = AppConf {
        window_title: "Hello There!".to_owned(),
        window_resolution: (800, 600),
    };

    let mut _app = match AppCore::init(conf) {
        Err(err) => {
            panic!(format!("Fatal error: {}", err));
        },
        Ok(app) => app,
    };

    _app.canvas.set_draw_color(Color::RGB(128, 0, 128));
    _app.canvas.clear();
    _app.canvas.present();

    sleep(Duration::from_secs(3));
}
